/** @Angular */
import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';

/** Services */
import { FontsService } from './services/fonts.service';
import { HistoryService } from './services/history.service';

/** Components */
import { FontsListComponent } from './fonts-list/fonts-list.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AppComponent implements AfterViewInit, OnDestroy {
  @ViewChild(FontsListComponent) fontsList: FontsListComponent;

  public currentFont: string;
  public currentSize = 16;
  public textEditor = {
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur consequatur est fuga minima neque officiis optio perferendis saepe sequi suscipit. Ducimus est eum facilis impedit laborum minima minus quia rem?',
    edit: false,
    pristine: true
  };

  private convertText: any;

  constructor(private changeDetector: ChangeDetectorRef, private fontsService: FontsService, private historyService: HistoryService) {
    this.fontsService.fontParams$.subscribe(
      newParams => { this.fontChange(newParams);}
    );
  }

  /**
   * Get element which contains testing text
   */
  ngAfterViewInit() {
    this.convertText = document.querySelector('#convertText');
  }

  ngOnDestroy() {
    this.fontsService.fontParams$.unsubscribe();
  }

  /**
   * Change text which show like an example and close text editor
   */
  public changeText() {
    this.textEditor['edit'] = false;
    this.textEditor['pristine'] = false;
    this.changeDetector.detectChanges();
  }

  // TODO: edit function comment
  /**
   * Change styles of the text for testing and add font into history
   *
   * @param fontFamily {String} Name of the selected font
   * @param fontSize   {Number} New font size for testing text
   * @param fontStyle  {String} Style of the selected font
   * @param fontWeight {String} Weight of the selected font
   */
  public fontChange(params) {
    if (params.fontFamily) {
      this.currentFont = params.fontFamily;
      this.convertText.style.fontFamily = params.fontFamily;

      this.historyService.changeHistory(params.fontFamily);
    }

    if (params.fontSize ) {
      this.currentSize = params.fontSize;
      this.convertText.style.fontSize = `${params.fontSize}px`;
    }

    params.fontWeight && (this.convertText.style.fontWeight = params.fontWeight);
    params.fontStyle && (this.convertText.style.fontStyle = params.fontStyle);
  }

  /**
   * Find page with current font
   *
   * @param {String} fontName Font name
   */
  public findFont(fontName) {
    this.fontsService.findFont(fontName);
  }
}
