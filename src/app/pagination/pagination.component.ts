/** @Angular */
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

/** Services */
import { PaginationService } from '../services/pagination.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})

export class PaginationComponent implements OnInit {
  @Input() paging;
  @Output() onPage = new EventEmitter<number>();

  constructor(private paginationService: PaginationService) { }

  ngOnInit() { }

  public setPage(page) {

    if (page < 1 || page > this.paging.total) {
      return;
    }

    this.onPage.emit(page);
  }

}
