/** @Angular */
import { Component, Output, EventEmitter } from '@angular/core';

/** Services */
import { FontsService } from '../services/fonts.service';

@Component({
  selector: 'app-range-slider',
  templateUrl: './range-slider.component.html',
  styleUrls: ['./range-slider.component.scss']
})

export class RangeSliderComponent {

  public fontSize = 16;

  constructor(private fontsService: FontsService) { }

  public changeSize(fontSize) {
    this.fontsService.changeFontParams({fontSize});
  }
}
