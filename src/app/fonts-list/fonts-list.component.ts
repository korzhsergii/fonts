/** @Angular */
import { Component, ChangeDetectionStrategy, ChangeDetectorRef, OnInit, Input, Output, EventEmitter } from '@angular/core';

/** Services */
import { FontsService } from '../services/fonts.service';
import { PaginationService } from '../services/pagination.service';

/** Libraries */
import WebFont from 'webfontloader/webfontloader';

@Component({
  selector: 'app-fonts-list',
  templateUrl: './fonts-list.component.html',
  styleUrls: ['./fonts-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class FontsListComponent implements OnInit {
  @Input() textEditor;

  public currentFonts: Object[];
  public currentFont: string;
  public currentFamilies = [];
  public paging: any;
  public sortOptions: Object[];

  private sortType: string;
  private start: number;
  private end: number;
  private fonts: any;
  private reg: any;
  private params: string[];

  constructor(
    private changeDetector: ChangeDetectorRef,
    private fontsService: FontsService,
    private paginationService: PaginationService
  ) {
    /** private */
    this.start = 0;
    this.end = 12;
    this.fonts = {
      families: {}
    };

    /** public */
    this.paging = {
      page: 1,
      perpage: 12
    };
    this.sortOptions = [
      {
        name: 'Name',
        value: 'name',
        selected: true
      },
      {
        name: 'Popularity',
        value: 'popularity'
      },
      {
        name: 'Alpha',
        value: 'alpha'
      },
      {
        name: 'Date',
        value: 'date'
      },
      {
        name: 'Style',
        value: 'style'
      },
      {
        name: 'Trending',
        value: 'trending'
      }
    ];
  }

  ngOnInit() {
    this.loadFonts();

    this.fontsService.findFont$.subscribe(
      font => this.selectedFont(font)
    );

    this.fontsService.selectedFont$.subscribe(
      data => {
        this.setPage(data['page']);
        this.fontsService.changeFontParams({fontFamily: data['fontName']});
      }
    );
  }

  /**
   * Set current page of the fonts list and load fonts
   *
   * @param {Number} page Number of the needed page
   */
  public setPage(page) {
    this.paging['page'] = page;
    this.start = (page - 1) * this.paging['perpage'];
    this.end = page * this.paging['perpage'];

    this.webFontLoad();
  }

  /**
   * Apply font styles to the test text
   *
   * @param {Event} event
   * @param {String} fontFamily Font family name
   * @param {Number} fontSize Font size number
   * @param {String} fontStyle Font style type
   * @param {Number} fontWeight Font weight number
   * @param {String} variants Additional options of the font
   */
  public fontChange({fontFamily, fontSize, fontStyle, fontWeight, variants}) {
    fontFamily && (this.currentFont = fontFamily);
    this.reg = /^(\d+)(\D+)$|(^\D+$)/gi;
    variants && (this.params = this.reg.exec(variants));

    if (this.params) {
      fontWeight = this.params[1] || 'initial';
      fontStyle = this.params.length > 2 && this.params[2] || this.params[3];
    }
    else {
      fontWeight = variants;
      fontStyle = 'initial';
    }

    if (variants === 'regular' || fontStyle === 'regular'){
      fontWeight = 'initial';
      fontStyle = 'initial';
    }

    this.fontsService.changeFontParams({fontFamily, fontSize, fontStyle, fontWeight});
  }

  /**
   * Open/Close list of the font options if it exist
   *
   * @param {Event} event
   * @param {Object} font Object with font information
   */
  public toggleOptions(event, font) {
    event.stopPropagation();

    this.currentFonts.forEach((item) => {
      if (item['isOpen']) {
        item['isOpen'] = !item['isOpen'];
      }
      else {
        item['isOpen'] = item['family'] === font['family'];
      }
    });
  }

  /**
   * Find font in the current fonts and follow to page where this font is and apply font styles
   *
   * @param {String} fontFamily Font family name
   */
  public selectedFont(fontName) {
    let index = this.currentFamilies.indexOf(fontName);

    if (index > -1) {
      let page = Math.ceil((index + 1) / this.paging['perpage']);

      this.currentFont = fontName;

      this.fontsService.selectedFont(fontName, page);
    }
  }

  /**
   * Set current sort type and load data by this sort
   *
   * @param {String} sortType Name of the sort
   */
  public selectedSort(sortType) {
    this.sortType = sortType;
    this.loadFonts();
  }

  /**
   * Load fonts list if it doesn't be and save it to the variable
   */
  private loadFonts() {
    if (this.fonts[this.sortType]) {
      this.setPage(1);

      return;
    }

    this.fontsService.loadFonts(this.sortType).subscribe(
      (fonts) => {

        this.currentFamilies = [];
        this.fonts[this.sortType] = fonts;
        this.paging['total'] = Math.ceil(fonts.length / this.paging['perpage']);

        this.fonts['families'][this.sortType] = fonts.reduce((prev, curr) => {
          this.currentFamilies.push(curr['family']);

          let tmprName = `${curr['family']}:${curr['variants'].join()}`;

          return [...prev, tmprName];
        }, []);

        this.setPage(1);
      },
      (err) => console.warn(err)
    );
  }

  /**
   * Load fonts in current page and link it
   *
   * @param {Number} [start] Number of the start position in pagination
   * @param {Number} [end] Number of the start position in pagination
   */
  private webFontLoad(start?: number, end?: number) {
    if (start >= 0 && end) {
      this.start = start;
      this.end = end;
    }

    this.currentFonts = this.fonts[this.sortType].slice(this.start, this.end);

    WebFont.load({
      google: {
        families: this.fonts.families[this.sortType].slice(this.start, this.end)
      },
      fontactive: (font) => {
        this.currentFonts.forEach((item) => {
          if (!item['loaded']) {
            item['loaded'] = item['family'] === font;
          }
        });
        this.changeDetector.detectChanges();
      }
    });
  }
}
