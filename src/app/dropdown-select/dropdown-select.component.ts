/** @Angular */
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-dropdown-select',
  templateUrl: './dropdown-select.component.html',
  styleUrls: ['./dropdown-select.component.scss']
})

export class DropdownSelectComponent implements OnInit {
  @Input() options;
  @Input() label;

  @Output() onSelected = new EventEmitter();

  public selected: any;
  public expand: boolean;

  constructor() { }

  ngOnInit() {
    this.selected = this.options.filter(item => item.selected)[0];
    document.addEventListener('click', this.clickHandler.bind(this));
  }

  /**
   * Listen click events on the page and close dropdown list if click isn't be on the dropdown container
   *
   * @param {Event} event
   */
  private clickHandler(event) {
    if (this.expand) {
      let handle = false;
      let target = event.target;
      do {
        handle = target.classList.contains('dropdown-select-container');
        target = target.parentNode;
      } while (!handle && (target instanceof Element));

      this.expand = handle;
    }
  }

  /**
   * Select needed option and transmit option value to parent function
   *
   * @param {Object} option Object with option info
   */
  public selectOption(option) {
    this.selected = option;
    this.expand = false;
    this.onSelected.emit(option.value);
  }
}
