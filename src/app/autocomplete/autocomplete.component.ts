/** @Angular */
import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})

export class AutocompleteComponent implements OnInit {
  @Input() searchData;

  @Output() onSelected = new EventEmitter();

  public filteredList = [];
  public query: string;
  public isResult: boolean;

  constructor() { }

  ngOnInit () {
    document.addEventListener('click', this.closeFunction.bind(this));
  }

  private closeFunction(event) {
    if (this.isResult && this.filteredList.length > 0) {
      let handle = false;
      let target = event.target;
      do {
        handle = target.classList.contains('autocomplete-container');
        target = target.parentNode;
      } while (!handle && (target instanceof Element));

      this.isResult = handle;
    }
  }

  /**
   * Filter data list by query string
   */
  public filter() {
    if (this.query.length >= 1) {
      this.filteredList = this.searchData.filter((item) => {
        return item.toLowerCase().indexOf(this.query.toLowerCase().trim()) > -1;
      });
      this.isResult = true;
    }
    else {
      this.filteredList = [];
    }
  }

  /**
   * Select needed item and transmit value to the parent function
   *
   * @param {String} item Name of the list item
   */
  public selectedItem(item) {
    this.isResult = false;
    this.query = item;
    this.onSelected.emit(item);
  }
}
