/** @Angular */
import { Injectable } from '@angular/core';

/** Rxjs */
import { Subject } from 'rxjs/Rx';

@Injectable()
export class PaginationService {
  public setPagination$ = new Subject();
  public changePage$ = new Subject();

  constructor() { }

  public setPagination(totalPages, perPage, page?) {
    this.setPagination$.next({total: totalPages, perpage: perPage, page: page});
  }

  public changePage(page) {
    this.changePage$.next(page);
  }
}