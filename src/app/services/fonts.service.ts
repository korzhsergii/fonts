/** @Angular */
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

/** Rxjs */
import { Observable, Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class FontsService {
  public fontParams$ = new Subject();
  public findFont$ = new Subject();
  public selectedFont$ = new Subject();

  private options: any;
  private url;

  constructor(
    private http: Http
  ) {
    this.options = {
      params: {
        key: 'AIzaSyBLBeqjz4y-yYybCig6p1PMKnt9g4PLLNU'
      }
    };
    this.url = 'https://www.googleapis.com/webfonts/v1/webfonts';
  }

  /**
   * Load fonts
   *
   * @param {String} sortType Name of the type which must be loaded
   * @returns {Observable<R|T>} Return promise with fonts or error
   */
  public loadFonts(sortType?) {
    delete this.options['params']['sort'];

    if (sortType && sortType !== 'name') {
      this.options['params']['sort'] = sortType;
    }

    return this.http.get(this.url, this.options)
      .map( (res: Response) => res.json().items )
      .catch( (err) => Observable.throw(err) );
  }

  // TODO: need comment
  public changeFontParams(params) {
    this.fontParams$.next(params);
  }

  // TODO: need comment
  public findFont(fontName) {
    this.findFont$.next(fontName);
  }

  // TODO: need comment
  public selectedFont(fontName, page) {
    this.selectedFont$.next({fontName, page});
  }
}
