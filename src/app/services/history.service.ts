/** @Angular */
import { Injectable } from '@angular/core';

/** Rxjs */
import { Subject } from 'rxjs/Rx';

@Injectable()
export class HistoryService {
  public history$ = new Subject();

  constructor() {}

  public changeHistory(newItem) {
    this.history$.next(newItem);
  }
}
