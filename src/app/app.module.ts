/** @Angular */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

/** Components */
import { AppComponent } from './app.component';
import { FontsListComponent } from './fonts-list/fonts-list.component';
import { PaginationComponent } from './pagination/pagination.component';
import { RangeSliderComponent } from './range-slider/range-slider.component';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { DropdownSelectComponent } from './dropdown-select/dropdown-select.component';
import { HistoryListComponent } from './history-list/history-list.component';

/** Services */
import { FontsService } from './services/fonts.service';
import { HistoryService } from './services/history.service';
import { PaginationService } from './services/pagination.service';


@NgModule({
  declarations: [
    AppComponent,
    FontsListComponent,
    PaginationComponent,
    RangeSliderComponent,
    AutocompleteComponent,
    DropdownSelectComponent,
    HistoryListComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [
    FontsService,
    HistoryService,
    PaginationService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
