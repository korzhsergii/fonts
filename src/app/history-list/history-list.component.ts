/** @Angular */
import { Component, ChangeDetectionStrategy, ChangeDetectorRef, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';

/** Services */
import { HistoryService } from '../services/history.service';

@Component({
  selector: 'app-history-list',
  templateUrl: './history-list.component.html',
  styleUrls: ['./history-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class HistoryListComponent implements OnInit, OnDestroy {
  public history: object[] = [];

  @Output() onFind = new EventEmitter();

  constructor(private historyService: HistoryService, private changeDetector: ChangeDetectorRef) { }

  ngOnInit() {
    this.historyService.history$.subscribe(
      newItem => this.addToHistory(newItem)
    );
  }

  ngOnDestroy() {
    this.historyService.history$.unsubscribe();
  }

  private addToHistory(newItem) {
    !this.history.includes(newItem) && this.history.push(newItem);

    if (this.history.length >= 10) {
      this.history.shift();
    }

    this.changeDetector.detectChanges();
  }

  public findFont(fontName) {
    this.onFind.emit(fontName);
  }
}
